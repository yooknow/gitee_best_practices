# 利用 Hugo 和 GiteePages 搭建免费个人博客

## 为什么选择 Hugo

Hugo 是一款非常优秀的静态博客生成器，速度、部署、易用上都比 Jekyll、Hexo 做得更好。我自己的博客也是从 Hexo 转移到 Hugo 的，因为已经厌倦了 Hexo 的部署麻烦和达到一定数量的博文后，生成 Html 文件非常缓慢。

## 搭建学习前提

需要安装好 Git 程序，会用基本的 ```git``` 命令和配置 ```ssh key``` 即可。

## 新建一个仓库

以 Gitee 用户名为仓库名，这样做是为了让 Pages 链接表示简短明了，并开启 Pages 服务。

![repositorie-name.png](https://i.loli.net/2019/06/20/5d0afd8661ec391204.png)

![blog-url.png](https://i.loli.net/2019/06/20/5d0af31b0dc8442280.png)

## Hugo 部署

1. 下载 hugo.exe 文件。[Hugo 下载链接](https://github.com/gohugoio/hugo/releases)，选择相应的系统版本即可，我这边是 windows，所以选择 ```hugo_0.55.6_Windows-32bit.zip```，解压可以看到 hugo.exe 文件，把其配置到操作系统的 path 中即可。打开 cmd 命令输入 hugo version，提示相应的 hugo 版本则表明安装配置成功。

    ![cmd-hugo.png](https://i.loli.net/2019/06/20/5d0af7b0c2e5e51676.png)

2. 按住键盘 Shift 键右击打开命令窗口，执行 ```hugo new site bequt```，其中 ```bequt 改成你的用户名```，用于生成站点文件。
3. 利用 git clone 命令 Gitee 博客仓库到本地，并把第二步生成的所有文件夹和文件复制到该克隆仓库中。

    ![hugo-tree.png](https://i.loli.net/2019/06/20/5d0b2f8b360a850168.png)

4. Hugo 默认是不带 Theme 的，需要手动下载，我这边使用的主题是 [basics](https://github.com/arjunkrishnababu96/basics)，直接把下载好的压缩文件解压放到 Theme 文件夹中，并改名成 basics 即可。仓库的目录结构如下：
    ```
    bequt
    ├─ archetypes
    │	└─ default.md
    ├─ config.toml
    ├─ resources
    │	└─ _gen
    │	 	├─ assets
    │	 	└─ images
    └─ themes
        └─ basics
            ├─ LICENSE.md
            ├─ README.md
            ├─ archetypes
            ├─ images
            ├─ layouts
            ├─ static
            └─ theme.toml
    ```
5. 配置 config.toml 文件，其中 baseURL 修改成自己 Pages 地址。
    ```
    baseURL = "https://bequt.gitee.io"
    title = "Hugo"
    theme = "basics"    
    ```
6. 在仓库根目录下执行 ```hugo server``` 命令，打开浏览器输入 http://localhost:1313 即可打开网站。

    ![hugo-server.png](https://i.loli.net/2019/06/20/5d0b3c1406f9790467.png)

    ![local-web.png](https://i.loli.net/2019/06/20/5d0b3c6dcf31a87990.png)

7. 进入 content 文件夹中建立 post 文件夹，在 post 文件夹中创建博文，每一篇博文要加上博文名、日期等信息。每一个主题的 content 文件夹里的组织方式都不一样，可以参照每一个主题中的 exampleSite 文件夹。
    ```
    +++
    title = "Hello Hugo"
    date  = "2019-06-06"
    +++

    Hugo，一个简单易用强大的静态博客生成器。
    ```
8. 把本地克隆仓库通过 git push 推送到 Gitee 上，并更新 Pages，即可部署完毕。

码云可以说是部署静态博客最棒的平台，在部署 Hugo 的时候不需要在本地生成 public 文件，只需要直接推送，码云自动识别是 Jekyll、Hugo、Hexo 等部署方式。目前码云也推出了 Web IDE 服务，可以说只需要一次部署 Hugo，以后就可以直接在码云网站中利用 Web IDE 服务动态更新博文，便捷快速，码云大大的好。